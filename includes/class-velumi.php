<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.velumi.com
 * @since      1.0.0
 *
 * @package    Velumi
 * @subpackage Velumi/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Velumi
 * @subpackage Velumi/includes
 * @author     Velumi <hello@velumi.com>
 */
class Velumi
{

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Velumi_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct()
	{
		if (defined('VELUMI_VERSION')) {
			$this->version = VELUMI_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'velumi';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_cache_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Velumi_Loader. Orchestrates the hooks of the plugin.
	 * - Velumi_i18n. Defines internationalization functionality.
	 * - Velumi_Admin. Defines all hooks for the admin area.
	 * - Velumi_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies()
	{

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-velumi-loader.php';

		/**
		 * The class responsible for defining internationalization functionality.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-velumi-i18n.php';

		/**
		 * The class responsible for defining all actions for cache handling
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'cache/class-velumi-cache.php';

		$this->loader = new Velumi_Loader();
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Velumi_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale()
	{

		$plugin_i18n = new Velumi_i18n();

		$this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
	}

	/**
	 * Register all of the hooks related to cache
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_cache_hooks()
	{

		$plugin_cache = new Velumi_Cache($this->get_plugin_name(), $this->get_version());

		$this->loader->add_action('admin_enqueue_scripts', $plugin_cache, 'enqueue_styles');
		$this->loader->add_action('admin_enqueue_scripts', $plugin_cache, 'enqueue_scripts');
		$this->loader->add_action('admin_bar_menu', $plugin_cache, 'admin_bar_cache_item', 100);
		$this->loader->add_action('wp_ajax_cache_purge', $plugin_cache, 'ajax_cache_purge_callback');

		$this->loader->add_action('transition_post_status', $plugin_cache, 'post_published', 10, 3);
		$this->loader->add_action('pre_post_update', $plugin_cache, 'post_unpublished', 10, 2);
		$this->loader->add_action('post_updated', $plugin_cache, 'post_updated', 10, 3);
		$this->loader->add_action('wp_trash_post', $plugin_cache, 'post_trashed', 10);
	}


	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run()
	{
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name()
	{
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Velumi_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader()
	{
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version()
	{
		return $this->version;
	}
}
