<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.velumi.com
 * @since      1.0.0
 *
 * @package    Velumi
 * @subpackage Velumi/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Velumi
 * @subpackage Velumi/includes
 * @author     Velumi <hello@velumi.com>
 */
class Velumi_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
