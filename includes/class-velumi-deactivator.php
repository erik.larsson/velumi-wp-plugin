<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.velumi.com
 * @since      1.0.0
 *
 * @package    Velumi
 * @subpackage Velumi/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Velumi
 * @subpackage Velumi/includes
 * @author     Velumi <hello@velumi.com>
 */
class Velumi_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
