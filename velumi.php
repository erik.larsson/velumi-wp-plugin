<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.velumi.com
 * @since             1.0.0
 * @package           Velumi
 *
 * @wordpress-plugin
 * Plugin Name:       Velumi
 * Plugin URI:        http://www.velumi.com
 * Description:       Velumi helper tools
 * Version:           1.1.4
 * Author:            Velumi
 * Author URI:        http://www.velumi.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       velumi
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}
/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-velumi-activator.php
 */
function activate_velumi()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-velumi-activator.php';
    Velumi_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-velumi-deactivator.php
 */
function deactivate_velumi()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-velumi-deactivator.php';
    Velumi_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_velumi');
register_deactivation_hook(__FILE__, 'deactivate_velumi');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-velumi.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_velumi()
{

    $plugin = new Velumi();
    $plugin->run();
}
run_velumi();
