<?php

/**
 *
 * @link       http://www.velumi.com
 * @since      1.0.0
 *
 * @package    Velumi
 */

// If uninstall not called from WordPress, then exit.
if (!defined('WP_UNINSTALL_PLUGIN')) {
	exit;
}
