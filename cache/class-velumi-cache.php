<?php

/**
 * Cache functionality.
 *
 * @link       http://www.velumi.com
 * @since      1.0.0
 *
 * @package    Velumi
 * @subpackage Velumi/cache
 */

class Velumi_Cache
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/velumi-admin.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/velumi-admin.js', array('jquery'), $this->version, false);
    }

    /**
     * Register admin bar item
     *
     * @since    1.0.0
     */
    public function admin_bar_cache_item($admin_bar)
    {
        $admin_bar->add_menu(array(
            'id'    => 'velumi-admin',
            'title' => 'Velumi',
            'href'  =>  "#"
        ));
        $admin_bar->add_menu(array('parent' => 'velumi-admin', 'title' => 'Clear cache', 'id' => 'clear-cache', 'href' => '#', 'meta' => array('title' => 'Clear cache')));
    }

    function ajax_cache_purge_callback()
    {
        $response = $this->clear_cache();
        echo $response;
        wp_die();
    }

    public function clear_cache()
    {
        try {
            exec('velumi-clear-cache', $output, $result_code);
            if ($result_code != 0) {
                $response = $this->legacy_fallback();
            } else {
                $response = "Success: Cache is now cleared!!";
            }
        } catch (\Exception $e) {
            $response = $this->legacy_fallback();
        }
        return $response;
    }

    public function legacy_fallback()
    {
        $response = wp_remote_get(
            'https://space.velumi.com/api/clear-cache/' . getenv('SITE_ID'),
            array(
                'sslverify' => false,
                'timeout' => 5,
            )
        );

        return "Success: Cache is now cleared!";
    }


    public function post_published($new_status, $old_status, $post)
    {
        if ($new_status === $old_status || $this->purge_single_happened) {
            return;
        }

        if ('publish' === $new_status) {

            $this->clear_cache();
        }
    }

    public function post_unpublished($postId, $updated)
    {
        if ($this->purge_single_happened) {
            return;
        }

        $post_status = get_post_status($postId);
        if ($post_status !==  'publish') {
            return;
        }

        if (isset($updated['post_status']) && 'publish' !== $updated['post_status']) {
            $this->clear_cache();
        }
    }


    public function post_updated($postId, $post_after, $post_before)
    {
        if (wp_is_post_revision($postId) || wp_is_post_autosave($postId)) {
            return;
        }

        if ($post_after->post_status === 'publish' &&   $post_before->post_status === 'publish') {
            $this->clear_cache();
        }
    }

    public function post_trashed($postId)
    {
        if ($this->purge_single_happened) {
            return;
        }
        $this->clear_cache();
    }
}
